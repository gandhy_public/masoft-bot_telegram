<?php
class BittrexScheduler extends CI_Controller
{


    public function run()
    {
        error_reporting(0);
        $this->load->model('ConfigModel');
        $this->load->model('OrderModel');
        $data_config = $this->ConfigModel->allActive()->result();
        $data_order = $this->OrderModel->allActive()->result();
        $datamap = array();
        $pricemap = array();
        $botordermap = array();
        foreach ($data_config as $c){
            if(!array_key_exists($c->market,$pricemap) && $c->market != "GLOBAL"){
                $market = $this->bittrex->checkMarket($c->market);
                unset($market["message"]);
                unset($market["status"]);
                $pricemap[$c->market] = $market;
            }
            if(!array_key_exists("USERS",$datamap["u" . $c->user_id])){
                $datamap["u" . $c->user_id]["USERS"] = array("user_id" => $c->user_id);
                $datamap["u" . $c->user_id]["ORDERS"] = array();
            }
            $datamap["u" . $c->user_id][$c->market][$c->name] = $c->value;
        }
        foreach ($data_order as $o){
                array_push($datamap["u" . $o->user_id]["ORDERS"],array("uuid" => $o->uuid,"action" => $o->action,"market" => $o->market));
        }

        foreach ($datamap as $u){
            $ordermap = array();
            foreach ($this->bittrex->getOpenOrders($u["GLOBAL"][CONFIG_API_TOKEN],$u["GLOBAL"][CONFIG_API_SECRET])->result as $o){
                array_push($ordermap ,$o->OrderUuid );
            }
            foreach ($u["ORDERS"] as $o){
                if(!in_array($o["uuid"],$ordermap)){
                    switch ($o["action"]){
                        case ACTION_BUY:
                            $qty = $u[$o["market"]]["buy-amount"];
                            $rate = (((float)$u[$o["market"]]["buy-at"])/((float)$pricemap[$o["market"]]["price"]))*100;
                            $actres = $this->bittrex->buyLimit($u["GLOBAL"][CONFIG_API_TOKEN],$u["GLOBAL"][CONFIG_API_SECRET],$o["market"],$qty,$rate);
                            $this->OrderModel->user_id =$u["USERS"]["user_id"];
                            $this->OrderModel->uuid =   $actres->result->uuid;
                            $this->OrderModel->market = $o['market'];
                            $this->OrderModel->action = ACTION_BUY;
                            $this->OrderModel->status = STATUS_ACTIVE;
                            $this->OrderModel->created = date('Y-m-d H:i:s');
                            $this->OrderModel->insertOrder();
                            break;
                        case ACTION_SELL:
                            $qty = $u[$o["market"]]["sell-amount"];
                            $rate = (((float)$u[$o["market"]]["sell-at"])/((float)$pricemap[$o["market"]]["price"]))*100;
                            $actres = $this->bittrex->sellLimit($u["GLOBAL"][CONFIG_API_TOKEN],$u["GLOBAL"][CONFIG_API_SECRET],$o["market"],$qty,$rate);
                            $this->OrderModel->user_id =$u["USERS"]["user_id"];
                            $this->OrderModel->uuid =   $actres->result->uuid;
                            $this->OrderModel->market = $o['market'];
                            $this->OrderModel->action = ACTION_SELL;
                            $this->OrderModel->status = STATUS_ACTIVE;
                            $this->OrderModel->created = date('Y-m-d H:i:s');
                            $this->OrderModel->insertOrder();
                            break;
                        default;
                            $qty = "-1";
                            $rate = "-1";
                            echo "Aksi tidak ditemukan";
                            break;
                    }
                    $this->OrderModel->setStatusByUuid($o['uuid'],STATUS_INACTIVE);
//                    $this->bot->send($u["USERS"]["user_id"],"Anda telah melakukan aksi " . $o["action"] . " pada " . $o["market"] . " sejumlah " . $qty . " dengan rate " . $rate . "%");
                    $this->bot->send("329528651","Anda telah melakukan aksi " . $o["action"] . " pada " . $o["market"] . " sejumlah " . $qty . " dengan rate " . $rate . "%");
                }else{
                    echo "sek onok";
                }
            }
        }

    }
}
?>