<?php
class ControllerBot extends CI_Controller
{

    public function index()
    {
        error_reporting(0);
        $data       = $this->bot->getData();
        $name       = $data->message->from->first_name;
        $username   = $data->message->from->username;
        $chat_id    = $data->message->chat->id;
        $chat_text  = strtolower($data->message->text);

        $menu = array(
            'Helo',
            '/start',
            'Start',
            'Setconfig',
            'Updateapi',
            'Stopconfig',
            'Showconfig',
        );

        switch ($chat_text){
            case (strpos($chat_text, ":") !== false):
                $this->route($chat_id,$chat_text,$name,$username);
                break;
            case ($this->strpos_arr(str_replace(" ", "", ucfirst($chat_text)), $menu) !== false):
                $func = "menu_".ucfirst(str_replace(" ", "", str_replace("/", "", $chat_text)));
                $this->resp_menu->$func($chat_id);
                break;

            /*OTHERS*/
            case ($chat_text == "cancel"):
                $this->cancel($chat_id);
                break;
            default:
                $this->bot->send($chat_id,"Maaf pesan yang Anda masukkan salah!");
        }
    }

    private function strpos_arr($haystack, $needle) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $what) {
            if(($pos = strpos($haystack, $what))!==false) return $pos;
        }
        return false;
    }

    private function cancel($chat_id)
    {
        $pesan      = "LOGOUT";
        $keyboard   = $this->bot->btn_menu();
        $this->bot->send($chat_id,$pesan,$keyboard);  
    }

    private function route($chat_id,$chat_text,$name,$username)
    {
        $satu = array(
            'market',
            'pricebuy',
            'amountbuy',
            'pricesell',
            'amountsell',
            'stopmarket',
        );
        $dua = array(
            'api',
            'secret',
            'updateapi',
            'updatesecret',
        );
        $route = array(
            'api' => array(
                'func' => 'set_api',
            ),
            'secret' => array(
                'func' => 'set_api',
            ),
            'updateapi' => array(
                'func' => 'set_update_api',
            ),
            'updatesecret' => array(
                'func' => 'set_update_api',
            ),
            'market' => array(
                'func' => 'set_config',
            ),
            'pricebuy' => array(
                'func' => 'set_config',
            ),
            'amountbuy' => array(
                'func' => 'set_config',
            ),
            'pricesell' => array(
                'func' => 'set_config',
            ),
            'amountsell' => array(
                'func' => 'set_config',
            ),
            'stopmarket' => array(
                'func' => 'set_stop_config',
            ),
        );

        $text = explode(" : ", $chat_text);
        if (count($text) == 3) {
            $text2 = explode("\n", $text[1]);
            $text2 = str_replace(" ", "", $text2[1]);
            $text  = str_replace(" ", "", $text[0]);

            switch (true) {
                case ($this->strpos_arr($text,$dua) !== false && $this->strpos_arr($text2,$dua) !== false):
                    $key = $route[$text]['func'];
                    $this->resp_input->$key($chat_id,$chat_text,$name,$username);
                    break;
                default:
                    $this->bot->send($chat_id,"Maaf pesan yang Anda masukkan salah!");
                    break;
            }
        }else{
            $text  = str_replace(" ", "", $text[0]);

            switch (true) {
                case ($this->strpos_arr($text,$satu) !== false):
                    $key = $route[$text]['func'];
                    $this->resp_input->$key($chat_id,$chat_text,$name,$username);
                    break;
                default:
                    $this->bot->send($chat_id,"Maaf pesan yang Anda masukkan salah!");
                    break;
            }
        }
    }
}
?>