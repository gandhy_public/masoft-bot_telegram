 <?php
class ControllerTes extends CI_Controller
{
    /*KEY VERGIE : 1bf5fcbcd8e3417582358d5b748d6647
    SECRET VERGIE : a4e8416e28ee4cea91402a5568d9ab74

    KEY GANDHY : 7c522605064f422899c8f550f6ca8e24
    SECRET GANDHY : f56214c499ab42c9aee5aad16408d0d3*/
    public function strpos_arr($haystack, $needle) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $what) {
            if(($pos = strpos($haystack, $what))!==false) return $pos;
        }
        return false;
    }

    public function cek()
    {
        $chat_text = "market : btc-ltc";
        $chat_id = 232286389;
        $name = "Gandhy";
        $username = "gandhy_bagoes";
        $satu = array(
            'market',
        );
        $dua = array(
            'api',
            'secret',
            'updateapi',
            'updatesecret',
        );
        $route = array(
            'api' => array(
                'func' => 'set_api',
            ),
            'secret' => array(
                'func' => 'set_api',
            ),
            'updateapi' => array(
                'func' => 'set_update_api',
            ),
            'updatesecret' => array(
                'func' => 'set_update_api',
            ),
            'market' => array(
                'func' => 'set_config',
            ),
        );

        $text = explode(" : ", $chat_text);
        if (count($text) == 3) {
            $text2 = explode("\n", $text[1]);
            $text2 = str_replace(" ", "", $text2[1]);
            $text  = str_replace(" ", "", $text[0]);

            switch (true) {
                case ($this->strpos_arr($text,$dua) !== false && $this->strpos_arr($text2,$dua) !== false):
                    $key = $route[$text]['func'];
                    $this->resp_input->$key($chat_id,$chat_text,$name,$username);
                    break;
                default:
                    $this->bot->send($chat_id,"Maaf pesan yang Anda masukkan salah!");
                    break;
            }
        }else{
            $text  = str_replace(" ", "", $text[0]);

            switch (true) {
                case ($this->strpos_arr($text,$satu) !== false):
                    $key = $route[$text]['func'];
                    $this->resp_input->$key($chat_id,$chat_text,$name,$username);
                    break;
                default:
                    $this->bot->send($chat_id,"Maaf pesan yang Anda masukkan salah!");
                    break;
            }
        }
    }

    public function coba()
    {
        $data="";
        for($a=1;$a<=5;$a++){
            if($a<10){$kode="000".$a;}
            else{$kode=$a;}
            $id = "232286389".$kode;
            $data_mem = new Block($id);
            $data .= $data_mem->read()."-";
        }
        /*$id = "2322863890002";
        $data_mem = new Block($id);
        echo $data_mem->read();*/
        echo $data;
    }

    public function tes()
    {
        $this->load->model("UserModel");
        $this->UserModel->id            = 2;
        $this->UserModel->name          = "vergie";
        $this->UserModel->username      = "vergieet";
        $this->UserModel->created       = date('Y-m-d H:i:s');
        $api_token     = "1bf5fcbcd8e3417582358d5b748d6647";
        $api_secret    = "a4e8416e28ee4cea91402a5568d9ab74";
        $reg = $this->UserModel->register($api_token,$api_secret);
        if(!$reg["status"]){
            echo $reg["message"];
        }else{
            echo "register success";
        }
    }

    public function tesSet()
    {
        $this->load->model("ConfigModel");
        $this->ConfigModel->user_id = "2";
        $this->ConfigModel->market  = "BTC-LTC";
        $this->ConfigModel->status  = STATUS_ACTIVE;
        $this->ConfigModel->created = date('Y-m-d H:i:s');
        $this->ConfigModel->name    = CONFIG_SELL_AMOUNT;
        $this->ConfigModel->value   = "3";
        $this->ConfigModel->saveConfig();
        $this->ConfigModel->name    = CONFIG_SELL_AT;
        $this->ConfigModel->value   = "5";
        $this->ConfigModel->saveConfig();
        $this->ConfigModel->name    = CONFIG_BUY_AMOUNT;
        $this->ConfigModel->value   = "3";
        $this->ConfigModel->saveConfig();
        $this->ConfigModel->name    = CONFIG_BUY_AT;
        $this->ConfigModel->value   = "3";
        $this->ConfigModel->saveConfig();
    }

    public function tesAddOrder()
    {
        $this->load->model("OrderModel");
        $this->OrderModel->user_id = "2";
        $this->OrderModel->market  = "BTC-LTC";
        $this->OrderModel->uuid  = "8925d746-bc9f-4684-b1aa-e507467aaa99";
        $this->OrderModel->action  = ACTION_BUY;
        $this->OrderModel->status  = STATUS_ACTIVE;
        $this->OrderModel->created = date('Y-m-d H:i:s');
        $this->OrderModel->insertOrder();
    }

    public function tesResetApiToken()
    {
        $this->load->model("ConfigModel");
        $this->ConfigModel->user_id = "2";
        //contoh salah
        //$res = $this->ConfigModel->resetApiTokenAndSecret("asd","asd");
        //contoh betul
        $res = $this->ConfigModel->resetApiTokenAndSecret("1bf5fcbcd8e3417582358d5b748d6647","a4e8416e28ee4cea91402a5568d9ab74");
        echo $res["message"];
    }

    public function tesCheckMarket()
    {
        //contoh benar
        $res = $this->bittrex->checkMarket("BTC-LTC");
        //contoh salah
        $res = $this->bittrex->checkMarket("BTS-LTC");
        echo $res["message"];
    }
    public function tesStopConfig()
    {
        $this->load->model("ConfigModel");
        print_r($this->ConfigModel->stopConfig("2","BTC-LTC"));
    }

    public function tesShowConfig()
    {
        $this->load->model("ConfigModel");
        $dataconfig = $this->ConfigModel->showConfig("232286389");
        unset($dataconfig["GLOBAL"]);//jika ingin mengexclude config global
        print("<pre>".print_r($dataconfig,true)."</pre>");
    }
    
}