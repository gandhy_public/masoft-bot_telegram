<?php
class ControllerBot extends CI_Controller
{

    public function index()
    {
        error_reporting(0);
        $data       = $this->bot->getData();
        $name       = $data->message->from->first_name;
        $username   = $data->message->from->username;
        $chat_id    = $data->message->chat->id;
        $chat_text  = strtolower($data->message->text);

        $need_api           = array('API:','Api:','api:');
        $need_secret        = array('SECRET:','Secret:','secret:');
        $update_api         = array('UPDATEAPI:','UpdateApi:','updateapi:');
        $update_secret      = array('UPDATESECRET:','UpdateSecret:','updatesecret:');
        $need_start         = array('/START','/Start','/start');

        $need_config    = array(
            'MARKET:','Market:','market:',
            'PRICEBUY:','PriceBuy:','pricebuy:',
            'AMOUNTBUY:','AmountBuy:','amountbuy:',
            'PRICESELL:','PriceSell:','pricesell:',
            'AMOUNTSELL:','AmountSell:','amountsell:',
        );

        $menu = array(
            'Helo',
            'Start',
            'setconfig',
            'Updateapi',
            'Stopconfig',
        );

        switch ($chat_text){
            case ($this->strpos_arr(str_replace(" ", "", $chat_text), $menu) !== false):
                $func = ucfirst(str_replace("/", "", $chat_text));
                $this->$func($chat_id);
                break;
            /*COMAND*/
            /*case ($this->strpos_arr($chat_text, $need_start) !== false):
                $this->cmd_api($chat_id);
                break;
            case ($chat_text == "UPDATE API"):
                $this->cmd_update_api($chat_id);
                break;
            case ($chat_text == "SET CONFIG"):
                $this->cmd_config($chat_id);
                break;
            case ($chat_text == "STOP CONFIG"):
                $this->cmd_stop($chat_id);*/

            /*ACTION*/
            case ($this->strpos_arr(str_replace(" ", "", $chat_text), $update_api) !== false && $this->strpos_arr(str_replace(" ", "", $chat_text), $update_secret) !== false):
                $this->set_update_api($chat_id,$chat_text,$name,$username);
                break;
            case ($this->strpos_arr(str_replace(" ", "", $chat_text), $need_api) !== false && $this->strpos_arr(str_replace(" ", "", $chat_text), $need_secret) !== false):
                $this->set_api($chat_id,$chat_text,$name,$username);
                break;
            case ($this->strpos_arr(str_replace(" ", "", $chat_text), $need_config) !== false):
                $this->set_config($chat_id,$chat_text,$name,$username);
                break;

            /*OTHERS*/
            case ($chat_text == "CANCEL"):
                $this->cancel($chat_id);
                break;
            default:
                $this->bot->send($chat_id,"Maaf pesan yang Anda masukkan salah!");
        }
    }

    private function strpos_arr($haystack, $needle) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $what) {
            if(($pos = strpos($haystack, $what))!==false) return $pos;
        }
        return false;
    }

    private function cancel($chat_id)
    {
        $pesan      = "LOGOUT";
        $keyboard   = $this->bot->btn_menu();
        $this->bot->send($chat_id,$pesan,$keyboard);  
    }

    public function Helo($chat_id,$chat_text)
    {
        $this->bot->send($chat_id,$chat_text);
    }


    /*FUNCTION UNTUK COMAND*/
    private function menu_Start($chat_id)
    {
        $keyboard   = $this->bot->btn_cancel();
        $pesan      = "Masukkan API KEY & API SECRET dengan format seperti dibawah ini \n\n API : {API KEY}\nSECRET : {API SECRET}";
        
        $this->bot->send($chat_id,$pesan,$keyboard);    
    }

    public function Updateapi($chat_id)
    {
        $this->bot->send($chat_id,"helooo...update api");die();
        $keyboard   = $this->bot->btn_cancel();
        $pesan      = "Masukkan API KEY & API SECRET baru dengan format seperti dibawah ini \n\n UPDATE API : {NEW API KEY}\nUPDATE SECRET : {NEW API SECRET}";
        
        $this->bot->send($chat_id,$pesan,$keyboard);    
    }

    public function setconfig($chat_id)
    {
        $keyboard   = $this->bot->btn_cancel();
        $pesan      = "Masukkan NAMA MARKET dengan format seperti dibawah ini \n\n MARKET : {nama market}\n\n<b>Example</b>\nMARKET : BTC-LTC";
        
        $this->bot->send($chat_id,$pesan,$keyboard);
    }

    private function menu_Stopconfig($chat_id)
    {
        $keyboard   = $this->bot->btn_cancel();
        $pesan      = "Masukkan NAMA MARKET yang akan dinon-aktifkan dengan format seperti dibawah ini \n\n STOP MARKET : {nama market}\n\n<b>Example</b>\nSTOP MARKET : BTC-LTC";
        
        $this->bot->send($chat_id,$pesan,$keyboard);
    }


    /*FUNCTION UNTUK SETTINGS*/
    private function set_api($chat_id,$chat_text,$name,$username)
    {
        $text = explode("\n", str_replace(" ", "", $chat_text));

        $api = explode(":", $text[0]);
        $api = trim($api[1]);

        $secret = explode(":", $text[1]);
        $secret = trim($secret[1]);

        $this->load->model("UserModel");
        $this->UserModel->id            = $chat_id;
        $this->UserModel->name          = $name;
        $this->UserModel->username      = $username;
        $this->UserModel->created       = date('Y-m-d H:i:s');
        $reg = $this->UserModel->register($api,$secret);

        if(!$reg["status"]){
            $pesan = $reg["message"].". Ulangi dengan format seperti dibawah ini \n\n API : {API KEY}\nSECRET : {API SECRET}";
            $keyboard = $this->bot->btn_cancel();
        }else{
            $pesan = "Registrasi Success";
            $keyboard = $this->bot->btn_menu();
        }

        $this->bot->send($chat_id,$pesan,$keyboard);    
    }

    private function set_update_api($chat_id,$chat_text,$name,$username)
    {
        $text = explode("\n", str_replace(" ", "", $chat_text));

        $api = explode(":", $text[0]);
        $api = trim($api[1]);

        $secret = explode(":", $text[1]);
        $secret = trim($secret[1]);

        $this->load->model("ConfigModel");
        $this->ConfigModel->user_id = $chat_id;
        $reg = $this->ConfigModel->resetApiTokenAndSecret($api,$secret);

        if(!$reg["status"]){
            $pesan = $reg["message"].". Ulangi dengan format seperti dibawah ini \n\n API : {API KEY}\nSECRET : {API SECRET}";
            $keyboard = $this->bot->btn_cancel();
        }else{
            $pesan = "Registrasi Success";
            $keyboard = $this->bot->btn_menu();
        }

        $this->bot->send($chat_id,$pesan,$keyboard);    
    }

    private function set_config($chat_id,$chat_text,$name,$username)
    {
        $config_array = array(
            '0001' => 'market',
            '0002' => 'pricebuy',
            '0003' => 'amountbuy',
            '0004' => 'pricesell',
            '0005' => 'amountsell'
        );

        $data         = str_replace(" ", "", $chat_text);
        $data         = explode(":", $data);

        $config_value = strtolower($data[1]);
        $config_name  = strtolower($data[0]);
        $config_code  = array_search($config_name, $config_array);

        if ($config_code == "0001") {
            $res = $this->bittrex->checkMarket($config_value);
            if ($res['status'] == true){
                $market_id = $chat_id.$config_code;
                $market = new Block($market_id);
                $market->write($config_value);

                $pesan = $res['message']."!\n"."Masukkan PRICE BUY dengan format seperti dibawah ini \n\n PRICE BUY : {price buy}";
            }else{
                $pesan = $res['message']." Atau ulangi setting MARKET";
            }
            $keyboard = $this->bot->btn_cancel();
        }elseif ($config_code == "0002") {
            $p_buy_id = $chat_id.$config_code;
            $p_buy = new Block($p_buy_id);
            $p_buy->write($config_value);

            $pesan = "Masukkan AMOUNT BUY dengan format seperti dibawah ini \n\n AMOUNT BUY : {amount buy}";
            $keyboard = $this->bot->btn_cancel();
        }elseif ($config_code == "0003") {
            $a_buy_id = $chat_id.$config_code;
            $a_buy = new Block($a_buy_id);
            $a_buy->write($config_value);

            $pesan = "Masukkan PRICE SELL dengan format seperti dibawah ini \n\n PRICE SELL : {price sell}";
            $keyboard = $this->bot->btn_cancel();
        }elseif ($config_code == "0004") {
            $p_sell_id = $chat_id.$config_code;
            $p_sell = new Block($p_sell_id);
            $p_sell->write($config_value);

            $pesan = "Masukkan AMOUNT SELL dengan format seperti dibawah ini \n\n AMOUNT SELL : {amount sell}";
            $keyboard = $this->bot->btn_cancel();
        }elseif ($config_code == "0005") {
            $a_sell_id = $chat_id.$config_code;
            $a_sell = new Block($a_sell_id);
            $a_sell->write($config_value);

            for($a=1;$a<=5;$a++){
                if($a<10){$kode="000".$a;}
                else{$kode=$a;}

                $id = $chat_id.$kode;
                $data_mem = new Block($id);

                if ($a == 1) {
                    $data_market = $data_mem->read();
                }elseif ($a == 2) {
                    $data_pricebuy = $data_mem->read();
                }elseif ($a == 3) {
                    $data_amountbuy = $data_mem->read();
                }elseif ($a == 4) {
                    $data_pricesell = $data_mem->read();
                }else {
                    $data_amountsell = $data_mem->read();
                }
                $data_mem->delete();
            }

            $this->load->model("ConfigModel");
            $this->ConfigModel->user_id = $chat_id;
            $this->ConfigModel->market  = $data_market;
            $this->ConfigModel->status  = STATUS_ACTIVE;
            $this->ConfigModel->created = date('Y-m-d H:i:s');
            $this->ConfigModel->name    = CONFIG_SELL_AMOUNT;
            $this->ConfigModel->value   = $data_amountsell;
            $this->ConfigModel->saveConfig();
            $this->ConfigModel->name    = CONFIG_SELL_AT;
            $this->ConfigModel->value   = $data_pricesell;
            $this->ConfigModel->saveConfig();
            $this->ConfigModel->name    = CONFIG_BUY_AMOUNT;
            $this->ConfigModel->value   = $data_amountbuy;
            $this->ConfigModel->saveConfig();
            $this->ConfigModel->name    = CONFIG_BUY_AT;
            $this->ConfigModel->value   = $data_pricebuy;
            $this->ConfigModel->saveConfig();
            $pesan = "Data berhasil disetting.\n\n<b>MARKET : </b>".$data_market."\n<b>PRICE BUY : </b>".$data_pricebuy."\n<b>AMOUNT BUY : </b>".$data_amountbuy."\n<b>PRICE SELL : </b>".$data_pricesell."\n<b>AMOUNT SELL : </b>".$data_amountsell;
            $keyboard = $this->bot->btn_menu();
        }else{

        }

        $this->bot->send($chat_id,$pesan,$keyboard);
    }

}
?>