<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curl {
    
    private $cookie_path = '';
    private $curl = [];

    public function init($key)
    {
        $this->curl = curl_init();
        $timestamp = (int) microtime(true);
        $this->cookie_path = __DIR__ . "/cookies_{$key}.txt";
    }

    public function send($url, $params = [], $type = 'GET', $timeout = 10000, $ssl = false)
    {
        $cr = $this->curl;
        if(!empty($params)) $build_param = http_build_query($params);
        else $build_param = '';        

        if($type == 'POST') {
            curl_setopt($cr, CURLOPT_POST, true);
            curl_setopt($cr, CURLOPT_POSTFIELDS, $build_param);
        } elseif($type == 'POST-RAW') {
            curl_setopt($cr, CURLOPT_POST, true);
            curl_setopt($cr, CURLOPT_POSTFIELDS, $params);
        } elseif($type == 'GET' && !empty($build_param)) {
            $url .= '?' . $build_param;
        }

        curl_setopt($cr, CURLOPT_URL, $url);
        curl_setopt($cr, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($cr, CURLOPT_USERAGENT, "Mozilla/7.0 (compatible; MSIE 9.0; Windows NT 5.1)");
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cr, CURLOPT_SSL_VERIFYPEER, $ssl);
        curl_setopt($cr, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($cr, CURLOPT_COOKIESESSION, true);
        curl_setopt($cr, CURLOPT_COOKIEJAR, $this->cookie_path);

        $response = curl_exec($cr);
        return $response;
    }

    public function close()
    {
        @unlink($this->cookie_path);
    }
}