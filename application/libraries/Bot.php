<?php
class Bot
{
	private $url 	= "https://api.telegram.org/bot561196849:AAGlQLaXEf5nLIIHOpdtafo18WJ7XJA_wM0";
	private $curl	= [];

	function __construct()
	{
		$this->curl = new Curl();
		$this->curl->init('Bot');
	}

	public function send($chatId,$message,$keyboard = null){
		$url = $this->url . "/sendmessage";
        if(null == $keyboard){
            $parameters = array(
                'chat_id' => $chatId,
                'text' => $message,
                'parse_mode' => "html",
            );
        }else{
            $parameters = array(
                'chat_id' => $chatId,
                'text' => $message,
                'reply_markup' => $keyboard,
                'parse_mode' => "html",
            );
        }
        $content = $this->curl->send($url, $parameters, 'POST');
        return $content;
    }

    public function getData()
    {
    	return json_decode(file_get_contents("php://input"));
    }

    public function btn_menu()
    {
    	$keyboard = [
            'keyboard' => [
                [
                    ['text' => 'SET CONFIG', 'callback_data' => 'null'],
                    ['text' => 'SHOW CONFIG', 'callback_data' => 'null']
                ],
                [
                    ['text' => 'UPDATE API', 'callback_data' => 'null'],
                    ['text' => 'STOP CONFIG', 'callback_data' => 'null']
                ],
                [
                    ['text' => 'CANCEL', 'callback_data' => 'null']
                ]
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
            'selective' => true
        ];
        return json_encode($keyboard);
    }

    public function btn_cancel()
    {
        $keyboard = [
            'keyboard' => [
                [
                    ['text' => 'CANCEL', 'callback_data' => 'null'],
                    ['text' => '/START', 'callback_data' => 'null']
                ]
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
            'selective' => true
        ];
        return json_encode($keyboard);
    }
}
?>