<?php
class Resp_menu
{
	private $ci;

    function __construct()
    {
        // Assign by reference with "&" so we don't create a copy
        $this->ci = &get_instance();
    }

    public function menu_Helo($chat_id)
    {
        $this->ci->bot->send($chat_id,"ini menu helo");
    }

    public function menu_Start($chat_id)
    {
        $keyboard   = $this->ci->bot->btn_cancel();
        $pesan      = "Masukkan API KEY & API SECRET dengan format seperti dibawah ini\n\nAPI : {API KEY}\nSECRET : {API SECRET}";
        
        $this->ci->bot->send($chat_id,$pesan,$keyboard);
    }

    public function menu_Setconfig($chat_id)
    {
        $keyboard   = $this->ci->bot->btn_cancel();
        $pesan      = "Masukkan NAMA MARKET dengan format seperti dibawah ini \n\n MARKET : {nama market}\n\n<b>Example</b>\nMARKET : BTC-LTC";
        
        $this->ci->bot->send($chat_id,$pesan,$keyboard);
    }

    public function menu_Updateapi($chat_id)
    {
        $keyboard   = $this->ci->bot->btn_cancel();
        $pesan      = "Masukkan API KEY & API SECRET baru dengan format seperti dibawah ini\n\nUPDATE API : {NEW API KEY}\nUPDATE SECRET : {NEW API SECRET}";
        
        $this->ci->bot->send($chat_id,$pesan,$keyboard);
    }

    public function menu_Stopconfig($chat_id)
    {
        $keyboard   = $this->ci->bot->btn_cancel();
        $pesan      = "Masukkan NAMA MARKET yang akan dinon-aktifkan dengan format seperti dibawah ini \n\n STOP MARKET : {nama market}\n\n<b>Example</b>\nSTOP MARKET : BTC-LTC";
        
        $this->ci->bot->send($chat_id,$pesan,$keyboard);
    }

    public function menu_Showconfig($chat_id)
    {
    	$this->ci->load->model("ConfigModel");
        $dataconfig = $this->ci->ConfigModel->showConfig($chat_id);
        unset($dataconfig["GLOBAL"]);//jika ingin mengexclude config global
        $index = array_keys($dataconfig);
        for($a=0;$a<count($index);$a++){
        	$market = $index[$a];
        	$price_buy = $dataconfig[$market]['buy-at'];
        	$amount_buy = $dataconfig[$market]['buy-amount'];
        	$price_sell = $dataconfig[$market]['sell-at'];
        	$amount_sell = $dataconfig[$market]['sell-amount'];
        	$market = strtoupper($market);

        	$pesan .= "<b>$market</b> (AKTIF)\nPRICE BUY : $price_buy\nAMOUNT BUY : $amount_buy\nPRICE SELL : $price_sell\nAMOUNT SELL : $amount_sell\n\n";
        }
        
        $this->ci->bot->send($chat_id,$pesan,$keyboard);
    }
}
?>