<?php
class Resp_input
{
	private $ci;

    function __construct()
    {
        // Assign by reference with "&" so we don't create a copy
        $this->ci = &get_instance();
    }

    public function set_api($chat_id,$chat_text,$name,$username)
    {
        $text = explode("\n", str_replace(" ", "", $chat_text));

        $api = explode(":", $text[0]);
        $api = str_replace(" ", "", $api[1]);

        $secret = explode(":", $text[1]);
        $secret = str_replace(" ", "", $secret[1]);

        $this->ci->load->model("UserModel");
        $this->ci->UserModel->id            = $chat_id;
        $this->ci->UserModel->name          = $name;
        $this->ci->UserModel->username      = $username;
        $this->ci->UserModel->created       = date('Y-m-d H:i:s');
        $reg = $this->ci->UserModel->register($api,$secret);

        if(!$reg["status"]){
            $pesan = $reg["message"].". Ulangi dengan format seperti dibawah ini\n\nAPI : {API KEY}\nSECRET : {API SECRET}";
            $keyboard = $this->ci->bot->btn_cancel();
        }else{
            $pesan = "Registrasi Success";
            $keyboard = $this->ci->bot->btn_menu();
        }

        $this->ci->bot->send($chat_id,$pesan,$keyboard);
    }

    public function set_update_api($chat_id,$chat_text,$name,$username)
    {
        $text = explode("\n", str_replace(" ", "", $chat_text));

        $api = explode(":", $text[0]);
        $api = str_replace(" ", "", $api[1]);

        $secret = explode(":", $text[1]);
        $secret = str_replace(" ", "", $secret[1]);

        $this->ci->load->model("ConfigModel");
        $this->ci->ConfigModel->user_id = $chat_id;
        $reg = $this->ci->ConfigModel->resetApiTokenAndSecret($api,$secret);

        if(!$reg["status"]){
            $pesan = $reg["message"].". Ulangi dengan format seperti dibawah ini\n\nUPDATE API : {API KEY}\nUPDATE SECRET : {API SECRET}";
            $keyboard = $this->ci->bot->btn_cancel();
        }else{
            $pesan = "Update Api Success";
            $keyboard = $this->ci->bot->btn_menu();
        }

        $this->ci->bot->send($chat_id,$pesan,$keyboard);    
    }

    public function set_config($chat_id,$chat_text,$name,$username)
    {
        $config_array = array(
            '0001' => 'market',
            '0002' => 'pricebuy',
            '0003' => 'amountbuy',
            '0004' => 'pricesell',
            '0005' => 'amountsell'
        );

        $data         = explode(":", str_replace(" ", "", $chat_text));

        $config_value = strtolower($data[1]);
        $config_name  = strtolower($data[0]);
        $config_code  = array_search($config_name, $config_array);

        if ($config_code == "0001") {
            $res = $this->ci->bittrex->checkMarket($config_value);
            if ($res['status'] == true){
                $market_id = $chat_id.$config_code;
                $market = new Block($market_id);
                $market->write($config_value);

                $pesan = $res['message']."!\n"."Masukkan PRICE BUY dengan format seperti dibawah ini\n\nPRICE BUY : {price buy}";
            }else{
                $pesan = $res['message']." Atau ulangi setting MARKET";
            }
            $keyboard = $this->ci->bot->btn_cancel();
        }elseif ($config_code == "0002") {
            $p_buy_id = $chat_id.$config_code;
            $p_buy = new Block($p_buy_id);
            $p_buy->write($config_value);

            $pesan = "Masukkan AMOUNT BUY dengan format seperti dibawah ini\n\nAMOUNT BUY : {amount buy}";
            $keyboard = $this->ci->bot->btn_cancel();
        }elseif ($config_code == "0003") {
            $a_buy_id = $chat_id.$config_code;
            $a_buy = new Block($a_buy_id);
            $a_buy->write($config_value);

            $pesan = "Masukkan PRICE SELL dengan format seperti dibawah ini\n\nPRICE SELL : {price sell}";
            $keyboard = $this->ci->bot->btn_cancel();
        }elseif ($config_code == "0004") {
            $p_sell_id = $chat_id.$config_code;
            $p_sell = new Block($p_sell_id);
            $p_sell->write($config_value);

            $pesan = "Masukkan AMOUNT SELL dengan format seperti dibawah ini\n\nAMOUNT SELL : {amount sell}";
            $keyboard = $this->ci->bot->btn_cancel();
        }elseif ($config_code == "0005") {
            $a_sell_id = $chat_id.$config_code;
            $a_sell = new Block($a_sell_id);
            $a_sell->write($config_value);

            for($a=1;$a<=5;$a++){
                if($a<10){$kode="000".$a;}
                else{$kode=$a;}

                $id = $chat_id.$kode;
                $data_mem = new Block($id);

                if ($a == 1) {
                    $data_market = $data_mem->read();
                }elseif ($a == 2) {
                    $data_pricebuy = $data_mem->read();
                }elseif ($a == 3) {
                    $data_amountbuy = $data_mem->read();
                }elseif ($a == 4) {
                    $data_pricesell = $data_mem->read();
                }else {
                    $data_amountsell = $data_mem->read();
                }
                $data_mem->delete();
            }

            $this->ci->load->model("ConfigModel");
            $this->ci->ConfigModel->user_id = $chat_id;
            $this->ci->ConfigModel->market  = $data_market;
            $this->ci->ConfigModel->status  = STATUS_ACTIVE;
            $this->ci->ConfigModel->created = date('Y-m-d H:i:s');
            $this->ci->ConfigModel->name    = CONFIG_SELL_AMOUNT;
            $this->ci->ConfigModel->value   = $data_amountsell;
            $this->ci->ConfigModel->saveConfig();
            $this->ci->ConfigModel->name    = CONFIG_SELL_AT;
            $this->ci->ConfigModel->value   = $data_pricesell;
            $this->ci->ConfigModel->saveConfig();
            $this->ci->ConfigModel->name    = CONFIG_BUY_AMOUNT;
            $this->ci->ConfigModel->value   = $data_amountbuy;
            $this->ci->ConfigModel->saveConfig();
            $this->ci->ConfigModel->name    = CONFIG_BUY_AT;
            $this->ci->ConfigModel->value   = $data_pricebuy;
            $this->ci->ConfigModel->saveConfig();
            $pesan = "Data berhasil disetting.\n\n<b>MARKET : </b>".$data_market."\n<b>PRICE BUY : </b>".$data_pricebuy."\n<b>AMOUNT BUY : </b>".$data_amountbuy."\n<b>PRICE SELL : </b>".$data_pricesell."\n<b>AMOUNT SELL : </b>".$data_amountsell;
            $keyboard = $this->ci->bot->btn_menu();
        }else{

        }

        $this->ci->bot->send($chat_id,$pesan,$keyboard);
    }

    public function set_stop_config($chat_id,$chat_text,$name,$username)
    {
        $market = explode(":", $chat_text);
        $market = str_replace(" ", "", $market[1]);

        $this->ci->load->model("ConfigModel");
        $stop = $this->ci->ConfigModel->stopConfig($chat_id,$market);

        if(!$stop["status"]){
            $pesan = $stop["message"].". Atau ulangi dengan format seperti dibawah ini\n\nSTOP MARKET : {nama market}\n\n<b>Example</b>\nSTOP MARKET : BTC-LTC";
            $keyboard = $this->ci->bot->btn_cancel();
        }else{
            $pesan = "Stop Config Success";
            $keyboard = $this->ci->bot->btn_menu();
        }

        $this->ci->bot->send($chat_id,$pesan,$keyboard);
    }
}
?>