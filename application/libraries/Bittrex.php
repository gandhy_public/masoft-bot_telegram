<?php
/**
 * Created by PhpStorm.
 * User: vergieet
 * Date: 5/30/18
 * Time: 11:21 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Bittrex
{
    private $base_url = "https://bittrex.com/api/v1.1";


    public function getBalance($apikey,$apisecret){
        $nonce=time();
        $uri= $this->base_url . '/account/getbalance?currency=BTC&apikey='.$apikey.'&nonce='.$nonce;
        $sign=hash_hmac('sha512',$uri,$apisecret);
        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apisign:'.$sign));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $execResult = curl_exec($ch);
        return json_decode($execResult);
    }
    public function getOpenOrders($apikey,$apisecret){
        return json_decode('{
	"success" : true,
	"message" : "",
	"result" : [{
			"Uuid" : null,
			"OrderUuid" : "09aa5bb6-8232-41aa-9b78-a5a1093e0211",
			"Exchange" : "BTC-LTC",
			"OrderType" : "LIMIT_SELL",
			"Quantity" : 5.00000000,
			"QuantityRemaining" : 5.00000000,
			"Limit" : 2.00000000,
			"CommissionPaid" : 0.00000000,
			"Price" : 0.00000000,
			"PricePerUnit" : null,
			"Opened" : "2014-07-09T03:55:48.77",
			"Closed" : null,
			"CancelInitiated" : false,
			"ImmediateOrCancel" : false,
			"IsConditional" : false,
			"Condition" : null,
			"ConditionTarget" : null
		}, {
			"Uuid" : null,
			"OrderUuid" : "8925d746-bc9f-4684-b1aa-e507467aaa99",
			"Exchange" : "BTC-LTC",
			"OrderType" : "LIMIT_BUY",
			"Quantity" : 100000.00000000,
			"QuantityRemaining" : 100000.00000000,
			"Limit" : 0.00000001,
			"CommissionPaid" : 0.00000000,
			"Price" : 0.00000000,
			"PricePerUnit" : null,
			"Opened" : "2014-07-09T03:55:48.583",
			"Closed" : null,
			"CancelInitiated" : false,
			"ImmediateOrCancel" : false,
			"IsConditional" : false,
			"Condition" : null,
			"ConditionTarget" : null
		}, {
			"Uuid" : null,
			"OrderUuid" : "614c34e4-8d71-11e3-94b5-425861b86a2b6",
			"Exchange" : "BTC-LTC",
			"OrderType" : "LIMIT_BUY",
			"Quantity" : 100000.00000000,
			"QuantityRemaining" : 100000.00000000,
			"Limit" : 0.00000001,
			"CommissionPaid" : 0.00000000,
			"Price" : 0.00000000,
			"PricePerUnit" : null,
			"Opened" : "2014-07-09T03:55:48.583",
			"Closed" : null,
			"CancelInitiated" : false,
			"ImmediateOrCancel" : false,
			"IsConditional" : false,
			"Condition" : null,
			"ConditionTarget" : null
		}
	]
}');
        $nonce=time();
        $uri= $this->base_url . '/market/getopenorders?apikey=' . $apikey . '&market=BTC-LTC&nonce='.$nonce;
        $sign=hash_hmac('sha512',$uri,$apisecret);
        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apisign:'.$sign));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $execResult = curl_exec($ch);
        return json_decode($execResult);
    }

    public function sellLimit($apikey,$apisecret,$market,$quantity,$rate){
        return json_decode("{
            \"success\" : true,
            \"message\" : \"\",
            \"result\" : {
                    \"uuid\" : \"614c34e4-8d71-11e3-94b5-888888\"
                }
        }");
        $nonce=time();
        $uri= $this->base_url . '/market/selllimit?market='. $market .'&quantity='.$quantity.'&rate=' .$rate .'&apikey='.$apikey.'&nonce='.$nonce;
        $sign=hash_hmac('sha512',$uri,$apisecret);
        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apisign:'.$sign));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $execResult = curl_exec($ch);
        return json_decode($execResult);
    }

    public function buyLimit($apikey,$apisecret,$market,$quantity,$rate){
        return json_decode("{
            \"success\" : true,
            \"message\" : \"\",
            \"result\" : {
                    \"uuid\" : \"614c34e4-8d71-11e3-94b5-425861b86a2b6\"
                }
        }");
        $nonce=time();
        $uri= $this->base_url . '/market/buylimit?market='. $market .'&quantity='.$quantity.'&rate=' .$rate .'&apikey='.$apikey.'&nonce='.$nonce;
        $sign=hash_hmac('sha512',$uri,$apisecret);
        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apisign:'.$sign));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $execResult = curl_exec($ch);
        return json_decode($execResult);
    }

    public function checkMarket($market){
        $this->curl = new Curl();
        $this->curl->init('Bittrex');
        $res = $this->curl->send($this->base_url . "/public/getmarketsummary?market=" . strtolower($market), [], 'POST');
        $res = json_decode($res);
        if(!$res->success)return array("status"=>$res->success,"message" => $this->convertMessage($res->message));
        return array("status"=>$res->success,"message" => "Market Ditemukan","price" => $res->result[0]->Last,"market_name" => $res->result[0]->MarketName);
    }

    public function  convertMessage($message){
        switch ($message){
            case "APIKEY_INVALID":
                return "Registrasi Gagal, pastikan API TOKEN & API SECRET Anda valid";
            case "INVALID_MARKET":
                return "Market yang anda pilih tidak tersedia, mohon periksa kembali.";
            default :
                return $message;
        }
    }
}