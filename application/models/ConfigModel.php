<?php
class ConfigModel extends CI_Model
{
    public $user_id;
    public $name;
    public $value;
    public $market;
    public $status;
    public $created;

    public function resetApiTokenAndSecret($api_token,$api_secret)
    {
        $data = $this->bittrex->getBalance($api_token,$api_secret);
        if($data->success) {
            $this->market = MARKET_GLOBAL;
            $this->name = CONFIG_API_TOKEN;
            $this->value = $api_token;
            $this->status = STATUS_ACTIVE;
            $this->created = date('Y-m-d H:i:s');
            $this->saveConfig();
            $this->name = CONFIG_API_SECRET;
            $this->value = $api_secret;
            $this->saveConfig();
            $data->message = "Berhasil merubah api token Anda";
        }
        return array("status"=>$data->success,"message"=>$this->bittrex->convertMessage($data->message));

    }

    public function saveConfig()
    {
        $this->db->set('status',STATUS_INACTIVE)
                ->where('user_id', $this->user_id)
                ->where('name', $this->name)
                ->where('market', $this->market)
                ->update("configs");
        $this->db->insert('configs', $this);
    }

    public function stopConfig($user_id,$market){
        if(strtoupper($market)== "GLOBAL")  return array("status"=>0,"message"=>"Cannot edit this market");

        $cek = $this->checkConfigAktif($user_id,$market);
        if (empty($cek)) {
            return array("status"=>0,"message"=>"Market tidak tersedia dalam config, silihkan melihat semua config yang aktif di SHOW CONFIG");
        }else{
            $this->db->set('status',STATUS_INACTIVE)
                ->where('user_id', $user_id)
                ->where('market', $market)
                ->update("configs");
            return array("status"=>1,"message"=>"Success");
        }
    }

    public function checkConfigAktif($user_id,$market)
    {
        return $this->db->where('user_id', $user_id)->where('status', STATUS_ACTIVE)->where('market', $market)->get("configs")->result_array();
    }

    public function showConfig($user_id){
        $data_config =  $this->db->where('user_id', $user_id)->where('status',STATUS_ACTIVE)->get("configs")->result();
        $datamap = array();
        foreach ($data_config as $c){
            $datamap[$c->market][$c->name] = $c->value;
        }
        return $datamap;
    }

    public function saveConfigOld()
    {
        if($this->checkConfigExist($this->user_id,$this->name)->num_rows()>0){
            $this->db->where('user_id', $this->user_id)->where('name', $this->name)->update("configs",$this);
        }else{
            $this->db->insert('configs', $this);
        }
    }

    public function find($id)
    {
        return $this->db->where('id', $id)->get("configs");
    }

    public function checkConfigExist($user_id,$name,$market=null)
    {
        if (null != $market) {
            return $this->db->where('user_id', $user_id)->where('name', $name)->where('market', $market)->get("configs");
        } else {
            return $this->db->where('user_id', $user_id)->where('name', $name)->get("configs");
        }
    }

    public function allActive()
    {
        return $this->db->where('status', STATUS_ACTIVE)->get("configs");
    }

}
?>