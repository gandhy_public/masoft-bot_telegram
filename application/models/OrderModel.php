<?php
class OrderModel extends CI_Model
{
    public $user_id;
    public $uuid;
    public $market;
    public $action;
    public $status;
    public $created;

    public function insertOrder()
    {
        $this->db->insert('orders', $this);
    }
    public function allActive(){
        return $this->db->where('status',STATUS_ACTIVE)->get("orders");
    }

    public function setStatusByUuid($uuid,$status)
    {
        $this->db->set("status",$status)->where('uuid', $uuid)->update("orders");
    }

}
?>