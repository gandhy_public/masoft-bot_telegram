<?php
class UserModel extends CI_Model
{
    public $id;
    public $name;
    public $username;
    /* Register User
     * @return boolean of status
     * */
    public function register($api_token,$api_secret)
    {
        if(null == $this->id){
            return array("status"=>false,"message"=>"Id tidak boleh kosong");
        }
        if($api_token == null){
            return array("status"=>false,"message"=>"Api Token tidak boleh kosong");
        }
        if($api_secret == null){
            return array("status"=>false,"message"=>"Api Secret tidak boleh kosong");
        }
        if($this->find($this->id)->num_rows()>0){
            return array("status"=>false,"message"=>"Duplicate Id");
        }
        $data = $this->bittrex->getBalance($api_token,$api_secret);
//        print_r($data);
        if($data->success){
            $this->db->insert('users', $this);
            $this->load->model("ConfigModel");
            $this->ConfigModel->user_id = $this->id;
            $this->ConfigModel->market = MARKET_GLOBAL;
            $this->ConfigModel->name = CONFIG_API_TOKEN;
            $this->ConfigModel->value = $api_token;
            $this->ConfigModel->status = STATUS_ACTIVE;
            $this->ConfigModel->created = date('Y-m-d H:i:s');
            $this->ConfigModel->saveConfig();
            $this->ConfigModel->name = CONFIG_API_SECRET;
            $this->ConfigModel->value = $api_secret;
            $this->ConfigModel->saveConfig();

        }
        return array("status"=>$data->success,"message"=>$this->bittrex->convertMessage($data->message));
    }

    /* Find User
     * @return array of users
     * */
    public function find($id)
    {
        return $this->db->where('id', $id)->get("users");
    }

    /* Find User
     * @return array of users
     * */
    public function findByUsername($username)
    {
        return $this->db->where('username', $username)->get("users");
    }

    public function all()
    {
        return $this->db->get("users");
    }
}
?>